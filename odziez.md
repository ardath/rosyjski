### odzież / одежда

sukienka                    - платье

spódniczka                  - юбка

rajstopy                    - рейтузы

bluzka                      - блузка

płaszcz                     - пальто

sweter                      - свитер

kapelusz                    - шляпа

pasek                       - поясок

obuwie                      - обувь


spodenki                    - брюки

podkoszulek                 - гимнастёрка

krawat                      - галстук

spodnie                     - штаны

buty                        - ботинки

czapka                      - шапка

koszula                     - рубашка

kurtka                      - куртка

skarpetki                   - носки

rękawiczki                  - перчатки

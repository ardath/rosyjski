Deklinacja I
============

# Do I deklinacji należą rzeczowniki rodzaju żeńskiego:

 – które w mianowniku liczby pojedynczej zakończone są na -a lub -я, машина (samochód), семья (rodzina);

 – których temat kończy się na -г, -к, -х – дорога (droga), книга (książka);

 – których temat kończy się na -ж, -ш, -ц, -ч, -щ – каша (kasza), улица (ulica);

 – rzeczowniki rodzaju męskiego, które w mianowniku liczby pojedynczej zakończone są na -a lub -я – мужчина (mężczyzna).


# Odmiana rzeczowników rodzaju żeńskiego twardotematowych zakończonych na -a, miękkotematowych zakończonych na -я, typu: картина (obraz), песня (pieśń).

Падеж        	 Единственное число    	  Множественное число
-------------------------------------------------------------
Именительный 	 сестра       	          сёстры

Родительный  	 сестры       	          сестёр

Дательный    	 сестре       	          сёстрам

Винительный  	 сестру       	          сестёр

Творительный 	 сестрой      	          сёстрами

Предложный   	 сестре       	          сёстрах


Падеж        	 Единственное число   	  Множественное число
-------------------------------------------------------------
Именительный 	 песня        	          песни

Родительный  	 песни        	          песен

Дательный    	 песне        	          песням

Винительный  	 песню        	          песни

Творительный 	 песней       	          песнями

Предложный   	 песне        	          песнях


# Odmiana rzeczowników rodzaju żeńskiego zakończonych na -г, -х, typu: подруга (koleżanka), муха (mucha).

Падеж        	 Единственное число  	    Множественное   число
-------------------------------------------------------------
Именительный 	 муха         	          мухи

Родительный  	 мухи         	          мух

Дательный    	 мухе         	          мухам

Винительный  	 муху         	          мух

Творительный 	 мухой        	          мухами

Предложный   	 мухе         	          мухах



# Odmiana rzeczowników rodzaju żeńskiego o temacie zakończonym na -ц, -ч, typu: курица (kura), свеча (świeca).

Падеж        	 Единственное число       Множественное число
-------------------------------------------------------------
Именительный 	 свеча        	          свечи

Родительный  	 свечи        	          свеч/свечей

Дательный    	 свече        	          свечам

Винительный  	 свечу        	          свечи

Творительный 	 свечой       	          свечами

Предложный   	 свече        	          свечах



Deklinacja II
=============

# Do II deklinacji należą rzeczowniki rodzaju męskiego:

 – które w mianowniku liczby pojedynczej zakończone są na spółgłoskę twardą – стол (stół);

 – których temat kończy się na -г, -к, -х – язык (język);

 – których temat kończy się na -ж, -ш, -ц, -ч, -щ – муж (mąż);

 – które w mianowniku liczby pojedynczej zakończone są na -ь – день (dzień);

 – które w mianowniku liczby pojedynczej zakończone są na -ий, -ой, -ай – чай (herbata);


 a także rodzaju nijakiego:

 – które w mianowniku liczby pojedynczej mają końcówki -o, -e lub -ё море (morze);

 – których temat kończy się na -ж, -ш, -ц, -ч, -щ – кольцо (pierścionek);

 – które w mianowniku liczby pojedynczej zakończone są na -ие путешествие (podróż).



# Odmiana rzeczowników rodzaju męskiego, zakończonych na spółgłoskę twardą i miękką typu: стол (stół), учитель (nauczyciel).

Падеж        	 Единственное число     	  Множественное число
-------------------------------------------------------------
Именительный 	 стол         	            столы

Родительный  	 стола        	            столов

Дательный    	 столу        	            столам

Винительный  	 стол         	            столы

Творительный 	 столом       	            столами

Предложный   	 столе        	            столах


Падеж        	 Единственное число     	  Множественное число
-------------------------------------------------------------
Именительный 	 учитель      	            учителя

Родительный  	 учителя      	            учителей

Дательный    	 учителю      	            учителям

Винительный  	 учителя      	            учителей

Творительный 	 учителем     	            учителями

Предложный   	 учителе      	            учителях


# Odmiana rzeczowników rodzaju męskiego zakończonych na spółgłoskę twardą oraz tych, których temat kończy się na -ж, -ц, typu: нож (nóż), певец (śpiewak).

Падеж        	 Единственное число  	      Множественное число
-------------------------------------------------------------
Именительный 	 нож          	            ножи

Родительный  	 ножа         	            ножей

Дательный    	 ножу         	            ножам

Винительный  	 нож          	            ножи

Творительный 	 ножем        	            ножами

Предложный   	 ноже         	            ножах


# Odmiana rzeczowników rodzaju nijakiego zakończonych na -о, -е, typu: окно (okno), поле (pole).

Падеж        	 Единственное число  	      Множественное число
-------------------------------------------------------------
Именительный 	 окно         	            окна

Родительный  	 окна         	            окон

Дательный    	 окну         	            окнам

Винительный  	 окно         	            окна

Творительный 	 окном        	            окнами

Предложный   	 окне         	            окнах


# Odmiana rzeczowników rodzaju nijakiego, których temat kończy się na -ч, -ц, typu: плечо (ramię), сердце (serce).

Падеж        	 Единственное число    	    Множественное число
-------------------------------------------------------------
Именительный 	 сердце       	            сердца

Родительный  	 сердца       	            сердец

Дательный    	 сердцу       	            сердцам

Винительный  	 сердце       	            сердца

Творительный 	 сердцем      	            сердцами

Предложный   	 сердце       	            сердцах

 czasowniki - глаголы
----------------------

być                               - быть

zaczynać                          - начинать

złamać, rozbić, połamać           - сломать

kupić                             - купить

zamykać                           - закрывать

przyjść                           - прийти

ciąć                              - риезать

tańczyć                           - танцевать

używać                            - употреблять

pić                               - пить

jeść                              - есть

spadać, upadać                    - падать

latać                             - летать

znajdować(się)                    - находить

naśladować                        - подразать _кому_

zapomnieć                         - забыть

dostać                            - получить

dawać                             - давать

mieć                              - у _кого_ есть

słyszeć                           - слушать

pomagać                           - помогать

skakać                            - прыгать

całować                           - целовать

śmiać się                         - смеяться

uczyć się                         - учиться чему

leżeć                             - лежать

kłamać                            - врать

lubić                             - любить

słuchać                           - слушать

patrzeć                           - смотреть

tracić                            - терять

kochać                            - любить

robić                             - делать

spotykać                          - встречать

poruszać się, przemieszczać się   - двигаться

potrzebować                       - нухдаться в чём

oferować                          - предлагать

otwierać                          - открывать

płacić                            - платить

grać, bawić się                   - играть

ciągnąć                           - тяеуть

naciskać                          - нажимать

kłaść                             - класть

czytać                            - читать

pamiętać                          - помнить

biegać                            - бегать

powiedzieć                        - сказать

sprzedawać                        - продавать

śpiewać                           - петь

siedzieć                          - сидеть

uśmiechać się                     - улыбаться

pływać                            - плавать

brać                              - брать

rozmawiać                         - разговаривать

rozumieć                          - понимать

używać                            - употреблять

czekać                            - ждать

pracować                          - работать

pisać                             - писать

obracać, odwracać się, skręcać    - враЩать

zostawać                          - оставлять

dzielić                           - делить

nauczać                           - учиться чему

iść                               - идти

podróżować                        - путешествовать

chodzić, spacerować               - гулять

odwiedzać                         - посеЩать

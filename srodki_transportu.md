#### СРЕДСТВА ТРАНСПОРТА / Środki transportu 

средства транспорта         - środki transportu

городской транспорт         - transport miejski

улица                       - ulica

тротуар                     - chodnik

переход                     - przejście dla pieszych

подземный переход           - przejście podziemne

светофор                    - światła

перекрёсток                 - skrzyżowanie

часы пик                    - godziny szczytu

пробки                      - korki

пешеход                     - pieszy

идти приамо                 - iść prosto

повернутъ направо           - skręcić w prawo

повернутъ налево            - skręcić w lewo

находится недалеко от чего  - znajduje się niedaleko

находится напротив          - znajduje się naprzeciw

ноходится около             - znajduje się w pobliżu

находится у                 - znajduje się przed

вход в метро                - wejście do metra

лестница                    - schody

эскалатор                   - schody ruchome

лифт                        - winda

подниматъся по эскалатору   - wejść shodami ruchomymi

спускатъся по эскалатору    - zejść schody ruchomymi

подниматъця на лифте        - wjechać windą

спускатъся на лифте         - zjechać windą

стоянка такси               - postój taksówek

остановка автобуса          - przystanek autobusowy

### Żywność / Питание


## gdzie można zjeść

bar z przekąskami                               - закусочная

bar z kanapkami                                 - бутербродная

stołówka                                        - столовая

kelner                                          - офицант

restauracja                                     - ресторан

bistro                                          - бистро



## potrawy

pierogi (takie jak polskie)                     - вариейники

trochę takie uszka                              - пелмени

tradycyjna zupa rybna                           - уха

inna tradycyjna zupa                            - солянка

rosół                                           - рассольник, булён

mięso z sosem                                   - мясо под соусом



rozmowa                                         - розговор





### na stole

kubek                                           - кружка

dzbanek do parzenia kawy                        - кофейник

koszyk do chleba                                - корзинка для хлеба

pieprz                                          - перец

pieprzniczka                                    - перечница

solniczka                                       - солонка

miseczka                                        - мусочка

spodek                                          - блюдце

łyżeczka                                        - ложечка

przyniósł                                       - прунёс

spieszyć się                                    - торопится

patelnia                                        - сковорода



# produkty spożywcze

szynka, wędlina                                 - ветина

parówki                                         - сосиски

pierś kurczaka                                  - куринная грудинка

przykro mi                                      - ксожалению

ogórki kwaszone                                 - квашенные огурцы

winogrono                                       - виноград

ser                                             - сыр

pęczek rzodkiewek                               - пучок редиски

puszka kawy                                     - банка кофе

karton mleka                                    - пакет молока

sałata                                          - салат

truskawka                                       - клубника

malina                                          - малина

ciastka                                         - пирожнос, печенье

jajka                                           - яйца

śmietana                                        - сметана

ziemniak                                        - картошка, картофель

twaróg                                          - творог

chleb                                           -

bułka

masło

mleko

sól

sok

цыплёнок?

ryż

kiełbasa

miód

dżem

ciasto

burak                                             - свекла

makaron                                           - лапша

sernik                                            - ватрушка

omlet                                             - хлопья

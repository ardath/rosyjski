### ОТПУСК / Urlop

набратъ ненужные вещи       - zabrać niepotrzebne rzeczy

взятъ толъко необходнимое   - wziąć niezbędne rzeczy

съездил на вокзал           - pojechał na dworzec

взял билеты                 - kupił bilety

билеты на прямой поезд      - bilety na pociąg bezpośredni

дeлат пересадку             - przesiadać się

иметъ скидку                - mieć zniżkę

студенческий билет          - bilet studencki

cообщтъ дату приезда        - poinformować o dacie przyjazdu

долгожданный денъ отъзда    - długo oczekiwany dzień wyjazdu

заказатъ такси              - zamówić taksówkę

поехатъ на вокзал           - pojechać na dworzec

зал ожидания                - poczekalnia

объявитъ поезд              - zapowiedzieć pociąg

предъявитъ билет            - okazać bilet

проводник                   - konduktor

входитъ в вагон             - wsiąść do wagonu

в купе                      - w przedziale

ждать                       - czekać

# rodzaje domów

dom wielorodzinny                     - многосемейный дом

dom jednorodzinny                     - особняк

kamienica                             - коменный дом

szeregowiec                           - таунхаус

dom wielopiętrowy                     - многоетажный дом

blok                                  - высотка



które piętro?                         - какой етаж?

na którym piętrze?                    - на каком етаже?



# mieszkanie

mała łazienka z prysznicem            - душевая

łazienka z wanną                      - ванная

sypialnia                             - cпальная, спяльня

gabinet                               - кабинет

pokój gościnny                        - гостинная комната, зал, салон

garderoba                             - гардероб

przedpokój                            - прихожая


# meble, wyposażenie

stół                                  - стол

łóżko                                 - крoват

komoda                                - комод

fotel                                 - кресло

lustro                                - зеркало

sofa, kanapa                          - диван

półka na książki                      - книжная полка

lampa na stół                         - настолная лампа


lodówka                               - холодильник

łyżka                                 - ложка

widelec                               - вилка

nóż                                   - нож

szklanka                              - стакан

talerz                                - тарелка

krzesło                               - стул

garnek, nocnik                        - горшок

doniczka                              - горшок для цветов

filiżanka                             - чашка для чая

telewizor                             - телевизор

dywan                                 - ковёр

okno                                  - окно

kołdra                                - одеяло

budzik                                - будильник

wieszak                               - вешалка

szafa                                 - шкаф

poduszka                              - подушка

drzwi                                 - дверь



brama 				                        - калитка

jadalnia 			                        - столовая




zlew                                  - раковина

ogród                                 - сад

piekarnik			                        - духовка

na dole                               - внизу

na górze                              - вверху

kominek                               - камин

kuchenka                              - плита

dzielnica                             - район

budynek administracyjny               - административное здание

pokój dziecięcy                       - детская комната

kawalerka                             - однокомнатка

odkurzacz                             - пылесос

kuchenka mikrofalowa                  - микровалновка

chata                                 - хата

garnek                                - кастрюла

basen                                 - бассейн

stół                                  - стол

mebel                                 - мебел

kran                                  - кран

strych                                - чердак

pralka                                - стиралная машина

czajnik                               - чайник

żelazko                               - утюг

prasować                              - гладит

umywalka                              - умывалник

zmywarka do naczyń                    - посудомойка

piwnica                               - подбал

tapeta                                - обой

balkon                                - балкон

szafka nocna                          - тумбочка

szafki kuchenne                       - кухонные шкафчики

zasłony                               - занавесы

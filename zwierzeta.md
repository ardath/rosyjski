# zwierzęta / животное

owady                             - насекомые

jeleń                             - олен

ptaszek                           - птичка

wiewiórka                         - белка

lis                               - лиса

kurka                             - курочка

kaczuszka/kaczka                  - уточка/утка

indyk                             - индюшка

motylek                           - бабочка

biedronka                         - божья коровка

pszczoły                          - пчёлы

kangur                            - конгуру

koza                              - коза

baran                             - баран

koń                               - лошад

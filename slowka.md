płakać                            - плакать

mamusia                           - мамачка

ciocia/ciocie                     - чёчя/чёч

spacer                            - прогулка

kuzyn                             - двоюродный

słońce                            - солнце

zima                              - зима

bałwan                            - сниеговик

                                  - сколзя?

lód                               - лед

                                  - ?срозат

łyżwy                             - коньки

lina                              - верёвка

ciągnąć                           - тянуть

sanki                             - санки

pożyczyć                          - одолжыть

                                  - скичяла

czwarta                           - четвортая

dorobić, popracować               - подрабатовать

w ten sposób                      - блaгодaря этому

starczyć                          - хватить

podobało się                      - поноравилоc

marzyć                            - мечтать

wrócić                            - вернуться

ulotka                            - листовкa

zdziwić się                       - удивиться

przed                             - раньше

głośno                            - громко

przydatny                         - полезное

huśtawka                          - кaчель

                                  - цчитать

rozwijać                          - рaзвивать

żartować                          - шутить

oszczędzić, uratować              - спасать

wystroić się                      - перeoдеться

zaklinować się, utknąć, zatrzymac - застрять

pojawić się                       - появиться

podróż                            - передвижения

dystans, zakres                   - расстяния

niedogodność                      - неудобство

zmęczenie                         - усталост

jazda samochodem                  - вождении машины

opalenizna                        - загор

kawałeczek                        - кусочек

zalety                            - преимущества

środek transportu                 - вид транспорта

niż                               - чем

w czasie wakacji                  - во время каникул

                                  - справочную аеропорта

schronisko                        - турбаза

kontrola celna                    - таможенный контрол

Oldze trzeba pomóc w nauce        - олге нужно помощь в учёбе

przewodnik                        - гид

ulepszyć                          - улучшaть

przyspieszony pociąg              - скорый поезд

peron                             - пут

tam i z powrotem                  - туда и обратно

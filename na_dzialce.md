# z czytanki 13 / Новые встречи

człowiek                        - член

uprawiać kwiaty                 - выращивать цветы

wokół                           - вокруг

zbierać się przy ognisku        - собираться у костра

smaczne potrawy                 - вкусные блуда

częstować                       - угощать

wracać                          - возвращаться

uważnie                         - внимателно

cały czas                       - всё время

podoba się                      - нравиться

cisza                           - тишина

wszędzie                        - везде



# inne:

duży                            - крупный

wynajmować                      - снимать

stąd                            - отсюда

sklep spożywczy                 - продуктовный магазин

prawie                          - почти

na obrzeżach                    - на окраине

budynek                         - здание

nie starczać, brakować          - не хватать

wydajwać się                    - кажеться

co sięcoco sięco się stało?     - что случилось?

zabłądzić                       - заблудиться

zgubić, stracić                 - потерять

trafić                          - попасть

szybciej                        - быстрее

jak dotrzeć?                    - как добраться?

lepiej                          - лучше

wysiąść                         - сойти

wsiąść do autobusu              - садиться на автобус

wzdłuż                          - вскрось

zobaczyć                        - увидит

przeprowadzać się               - переехать

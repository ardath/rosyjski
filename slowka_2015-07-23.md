obowiązki                   - повинности; обязательство; обязанность

śmietnik                    - мусорка

troszkę, trochę, coś niecoś - немножко

oczywiście                  - толково; естественно

pożyczyć                    - занять

starać się                  - стараться; силиться

czkawka                     - икота

przestraszyć się            - перепугаться

przerazić, nastraszyć       - испугать

domyślić się                - додуматься; догадаться

żeglować                    - плавать на парусниках

zwiedzać                    - посещать

dokładnie                   - на дробне

kościół                     - костЁл

poza domem                  - вне дома

siwe włosy                  - седне волосы

brunet                      - брюнет

wygląd zewnętrzny           - внемный вид

do kogo jesteś podobna?     - на кого ты похожа?

pociągła twarz              - лицо продолговате

wykapany tata               - просто вылетый отец

wzrost                      - рост

podpowiedź                  - совет; намек;

wiek                        - возраст

mylić                       - путать

odróżniać się               - отличаться

jak wygląda                 - как выглядеть

porównywać                  - сравнивать

szanować                    - уважать

bliźniaki                   - близнецы

na razie                    - пока

dostać, otrzymać            - получать

błyskawica                  - молния

dawny, były                 - бывший

cieńki                      - тонкий



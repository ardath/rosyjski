### в Москве

zatem                                           - итак

pojechaliśmy samochodem do nich do domu         - мы поехали на машине к ним домой

najpierw                                        - сначала

poszliśmy na Plac Czerwony                      - мы пошли на красную площадь

baszta, wieża                                   - башня

świątynia                                       - храм

zwiedzić                                        - посетить

dzwon                                           - колокол

wystawy obrazów                                 - выставки картин

śpiewają piosenki                               - поют песни

czytają wiersze                                 - читают стихи

galeria obrazów (malarstwa)                     - картинная галерея

malarz                                          - художник

spodobać się                                    - понравится

zaproponować                                    - предложить

czekała na nas niespodzianka                    - нас ожидал сюрприз

bilety do teatru                                - билеты в театр

na prawym brzegu rzeki Moskwy                   - на правом берегу москвы-реки

stadion                                         - стадион

korty tenisowe                                  - теннисные корты

przystanie z łódkami                            - лодочные станции

wiele atrakcji                                  - много аттракционов

ciekawie spędzilić czas                         - интерресно провелить время 

po spektaklu                                    - после спектакля

na brzegu rzeki                                 - на берегу реки

specjalność babci                               - специальность бабушки

pójdziemy na ryby                               - пойдём на рыбалку

tata złapał dużą rybę                           - тата поймал болшую рыбу 

przyszła pora pożegnania                        - пришла пора прощаться

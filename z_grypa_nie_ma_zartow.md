### Я ОТЛИЧНО СЕБЯ ЧУВСТВУЮ             - CZUJĘ SIĘ ŚWIETNIE


отлично                             - świetnie

хорошо                              - dobrze

наважно                             - kiepsko

плохо                               - źle

ужасно                              - okropnie



у миеня болит палец                 - boli mnie palec.

у миеня болят глаза, руки, ноги     - bolą mnie oczy, ręce, nogi.


я заболиел гроппом                  - zachorowałem na grypę

           ангиной                                    anginę

           воспалением лёгких                         zapalenie płuc


####  PRZY TELEFONIE:

## na początku:

- алло!                                 

- слушаю!                                 

- это я!                                  

- да, это я!                                  

## na zakończenie:

- до свидания!

- пока!

- хорошо, что ты позвонил!

- жду звонка!

- до встречи!


## przy pomyłce:

- вы ошиблис!

- вы не туда попали!



мне     надо (нужно) ехать (поехать) домой.  - muszę jechać do domu

тебе                                         - ty musisz

ему                                          - on musi

ей                                           - ona

нам                                          - my musimy

вам                                          - wy

им                                           - oni

маме (брату)                                 - mama musi



#### части тела человека  - części ciała

лицо                 - twarz

волосы               - włosy

лоб                  - czoło

бровь (брови)        - brew

глаз                 - oko

нос                  - nos

ухо (уши)            - ucho

щека (щёки)          - policzek

губы                 - wargi

шея                  - szyja

плечо (плечи)        - ramię

грудь                - pierś

рука (руки)          - ręka

локоть (локти)       - łokieć

палец (пальцы)       - palec

живот                - brzuch

нога (ноги)          - noga

колено (колени)      - kolano

рот                  - usta

попка                - dupcia
